﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notebook
{
    class Notebook
    {
        static void Main(string[] args)
        {
            int i = 1;
            while (i != 0)
            {
                Console.WriteLine("Здравствуйте, для начала работы с записной книжкой выберите одну из предложенных функций:");
                Console.WriteLine("Чтобы ввести новую заметку введите \"n\"");
                Console.WriteLine("Чтобы просмотреть краткую информацию о всех заметках сразу и узнать порядковый номер введите \"i\"");
                Console.WriteLine("Чтобы просмотреть полную информацию о нужной заметке введите \"r\"");
                Console.WriteLine("Чтобы удалить заметку введите \"d\"");
                Console.WriteLine("Чтобы редактировать заметку введите \"s\"");
                Console.WriteLine("Чтобы выйти из программы введите \"выход\"");
                string swcase = Console.ReadLine();
                switch (swcase)
                {
                    case "n":
                        Note.NewNote();
                        Console.WriteLine();
                        break;
                    case "i":
                        Note.EzView();
                        Console.WriteLine();
                        break;
                    case "r":
                        if (Note.EzView())
                            Note.View();
                        else
                            Console.WriteLine("Нет записей для просмотра");
                        Console.WriteLine();
                        break;
                    case "d":
                        if (Note.EzView())
                            Note.DelNote();
                        else
                            Console.WriteLine("Нет записей для удаления");
                        Console.WriteLine();
                        break;
                    case "s":
                        if (Note.EzView())
                            Note.EditNote();
                        else
                            Console.WriteLine("Нет записей для редактирования");
                        Console.WriteLine();
                        break;
                    case "выход":
                        Console.WriteLine("До свидания");
                        i = 0;
                        break;
                }
            }
        }
    }
}
