﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notebook
{
    class Note
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Middlename { get; set; }
        public int Phnum { get; set; }
        public string Country { get; set; }
        public DateTime Birth { get; set; }
        public string Org { get; set; }
        public string Level { get; set; }
        public string Other { get; set; }
        static public string str;
        public int Id { get; private set; }
        static public int num = 0;
        public static Dictionary<int, Note> pad = new Dictionary<int, Note>();

        public static void NewNote()
        {
            int pnum = 0;
            DateTime bDate;
            Console.WriteLine("Начнем создание новой заметки");
            Console.WriteLine("Обязательные поля помечены \"*\"");
            Note note = new Note();
            Console.Write("Введите фамилию* описываемого человека:          ");
            note.Surname = Console.ReadLine();
            while (String.IsNullOrEmpty(note.Surname) && note.Surname.Length < 1)
            {
                Console.Write("Попробуйте ввести фамилию ещё раз: ");
                note.Surname = Console.ReadLine();
            }
            Console.Write("Введите имя*:                                    ");
            note.Name = Console.ReadLine();
            while (String.IsNullOrEmpty(note.Name) && note.Name.Length < 1)
            {
                Console.Write("Попробуйте ввести имя ещё раз: ");
                note.Name = Console.ReadLine();
            }

            Console.Write("Введите очество:                                 ");
            str = Console.ReadLine();
            if (!String.IsNullOrEmpty(str) && str.Length > 1)
            {
                note.Middlename = str;
                str = null;
            }
            Console.Write("Введите номер телефона*:                         ");
            while (!int.TryParse(Console.ReadLine(), out pnum))
            {
                Console.WriteLine("Попробуйте ввести номер ещё раз");
            }
            note.Phnum = pnum;

            Console.Write("Введите страну*:                                 ");
            note.Country = Console.ReadLine();
            while (String.IsNullOrEmpty(note.Country) && note.Country.Length < 1)
            {
                Console.Write("Попробуйте ввести страну ещё раз: ");
                note.Country = Console.ReadLine();
            }
            Console.Write("Введите дату рождения в формате YYYY, MM, DD:    ");
            str = Console.ReadLine();
            if (!String.IsNullOrEmpty(str) && str.Length > 1)
            {
                while (!DateTime.TryParse(str, out bDate))
                {
                    Console.WriteLine("Попробуйте ввести дату рождения в формте YYYY, MM, DD ещё раз: ");
                    str = Console.ReadLine();
                }
                note.Birth = bDate;
                str = null;
            }
            Console.Write("Введите организацию:                             ");
            str = Console.ReadLine();
            if (!String.IsNullOrEmpty(str) && str.Length > 1)
            {
                note.Org = str;
                str = null;
            }
            Console.Write("Введите должность:                               ");
            str = Console.ReadLine();
            if (!String.IsNullOrEmpty(str) && str.Length > 1)
            {
                note.Level = str;
                str = null;
            }
            Console.Write("Введите дополнительную информацию:               ");
            str = Console.ReadLine();
            if (!String.IsNullOrEmpty(str) && str.Length > 1)
            {
                note.Other = str;
                str = null;
            }
            num += 1;
            note.Id = num;
            pad.Add(num, note);
            Console.Clear();

        }
        public static bool EzView()
        {
            Console.Clear();
            if (pad.Count == 0)
            {
                Console.WriteLine("Книжка пуста");                
                return false;
            }
            else
            {

                foreach (var a in pad)
                {
                    Console.WriteLine("Краткое описание записи: ");
                    Console.WriteLine("Порядковый номер " + a.Key);
                    Console.WriteLine("Фамилия          " + a.Value.Surname);
                    Console.WriteLine("Имя              " + a.Value.Name);
                    Console.WriteLine("Телефон          " + a.Value.Phnum);
                }
                return true;
            }


        }
        public static void View()
        {
            Console.WriteLine("Введите Порядковый номер записи, чтобы просмотреть");
            int index;
            while (!int.TryParse(Console.ReadLine(), out index))
            {
                Console.WriteLine("Попробуйте ввести ещё раз");
            }
            if (pad.ContainsKey(index))
            {


                Console.WriteLine("Описание записи: ");
                Console.WriteLine("Порядковый номер:" + index);
                Console.WriteLine("Фамилия:         " + pad[index].Surname);
                Console.WriteLine("Имя:             " + pad[index].Name);
                if (pad[index].Middlename != null)
                {
                    Console.WriteLine("Очество:         " + pad[index].Middlename);
                }
                Console.WriteLine("Телефон:         " + pad[index].Phnum);
                Console.WriteLine("Страна:          " + pad[index].Country);
                if (pad[index].Birth != null)
                {
                    Console.WriteLine("Дата рождения:   " + pad[index].Birth);
                }
                if (pad[index].Org != null)
                {
                    Console.WriteLine("Организация:     " + pad[index].Org);
                }
                if (pad[index].Level != null)
                {
                    Console.WriteLine("Должность:       " + pad[index].Level);
                }

                if (pad[index].Other != null)
                {
                    Console.WriteLine("Дополнительная информация: " + pad[index].Other);
                }
            }
            else
            {
                Console.WriteLine("Неверный порядковый номер, попробуйте сначала.");
            }
        }
        public static void DelNote()
        {
            Console.WriteLine("Введите порядковый номер записи, чтобы удалить");
            int index;
            while (!int.TryParse(Console.ReadLine(), out index))
            {
                Console.WriteLine("Попробуйте ввести ещё раз");
            }
            if (pad.ContainsKey(index))
            {
                pad.Remove(index);
            }
            else
            {
                Console.WriteLine("Неверный порядковый номер, попробуйте сначала.");
            }
        }
        public static void EditNote()
        {

            int pnum = 0;
            DateTime bDate;
            Console.WriteLine("Введите порядковый номер записи, чтобы редактировать");
            int index;
            while (!int.TryParse(Console.ReadLine(), out index))
            {
                Console.WriteLine("Попробуйте ввести ещё раз");
            }
            if (pad.ContainsKey(index))
            {


                Console.WriteLine("Если не желаете изменить запись нажмите Enter");
                Console.Write("Введите фамилию* описываемого человека:          ");
                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    pad[index].Surname = str;
                    str = null;
                }
                Console.Write("Введите имя*:                                    ");

                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    pad[index].Name = str;
                    str = null;
                }
                Console.Write("Введите очество:                                 ");

                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    pad[index].Middlename = str;
                    str = null;
                }
                Console.Write("Введите номер телефона*:                         ");

                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    while (!int.TryParse(str, out pnum))
                    {
                        Console.WriteLine("Попробуйте ввести номер ещё раз");
                        str = Console.ReadLine();
                    }
                    pad[index].Phnum = pnum;
                    str = null;
                }
                Console.Write("Введите страну*:                                 ");
                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    pad[index].Country = str;
                    str = null;
                }
                Console.Write("Введите дату рождения в формате YYYY, MM, DD:    ");
                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    while (!DateTime.TryParse(str, out bDate))
                    {
                        Console.WriteLine("Попробуйте ввести дату рождения в формте YYYY, MM, DD ещё раз: ");
                        str = Console.ReadLine();
                    }
                    pad[index].Birth = bDate;
                    str = null;
                }
                Console.Write("Введите организацию:                             ");
                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    pad[index].Org = str;
                    str = null;
                }
                Console.Write("Введите должность:                               ");
                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    pad[index].Level = str;
                    str = null;
                }
                Console.Write("Введите дополнительную информацию:               ");
                str = Console.ReadLine();
                if (!String.IsNullOrEmpty(str) && str.Length > 1)
                {
                    pad[index].Other = str;
                    str = null;
                }
            }
            else
            {
                Console.WriteLine("Неверный порядковый номер, попробуйте сначала.");
            }

        }
    }


}
